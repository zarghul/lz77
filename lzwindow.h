#ifndef __LZWINDOW_H__
#define  __LZWINDOW_H__

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

//node structure for the search tree
struct node
{
	ssize_t parent,left, right;
};
typedef struct node node_t;

//structure representing the sliding window and the lookahead buffer
//also contains binary search tree fields
struct lzwindow
{
	//sliding window lenght
	size_t wlen;
	//lookahead buffer lenght
	size_t llen;
	//lookahead buffer current size
	size_t lleft;
	//offset of the sliding window
	int off;
	//file to fetch from
	FILE* f;
	//sliding window
	uint8_t* win;

	//search tree fields
	ssize_t root;
	node_t* tree;
	size_t tree_size;
};
#define NULL_NODE -1

typedef struct lzwindow lzwindow_t;

//structure representing a search match
struct match
{
	int index;
	int len;
};

//function for struct initialization
//return false upon failure
bool lzwindow_init(FILE* f,size_t wlen, size_t llen,lzwindow_t* lz);

//function for structure cleanup
void lzwindow_free(lzwindow_t* lz);

//function for fetching values from the sliding window
//it performs a bound check, returns false upon failure
//(elements that fall into the lookahead buffer are valid)
bool lzwindow_getw(const lzwindow_t* lz, size_t index, uint8_t* s);

//function for fetching values from the lookahead buffer
//it performs a bound check, returns false upon failure
bool lzwindow_getl(const lzwindow_t* lz, size_t index, uint8_t* s);

//function for sliding the window, fetching the tail with the lh buffer
//it also updates internal search tree
//returns num or -1 if num > lz->llen
int lzwindow_advance(lzwindow_t* lz, size_t num);

//function for searching in the window for the longest lh buffer match
int lzsearch(lzwindow_t *lz,struct match* m);

#endif /* __LZWINDOW_H__ */
