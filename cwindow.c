#include <string.h>
#include <errno.h>

#include "cwindow.h"

#define DEBUG
#include "debug.h"

struct cwindow
{
	size_t len;
	size_t start;
	uint8_t* buf;
};

bool cwindow_alloc(cwindow_t* pw,size_t len,uint8_t* init)
{


	if(pw == 0)
	{
		errno = EINVAL;
		goto null_err;
	}
	*pw = (cwindow_t)calloc(1,sizeof(struct cwindow));
	if(*pw == 0)
	{
		errno = ENOMEM;
		goto calloc_err1;
	}
	(*pw)->buf = (uint8_t*)calloc(len,sizeof(uint8_t)); 
	if((*pw)->buf == 0)
	{
		errno = ENOMEM;
		goto calloc_err2;
	}
	(*pw)->len = len;
	
	(*pw)->start = 0;

	if(init != 0)
	{
		memcpy((*pw)->buf,init,len);
	}

	return true;

calloc_err2:
	free(*pw);
calloc_err1:
null_err:
	return false;
}

void cwindow_free(cwindow_t* pw)
{
	free((*pw)->buf);
	free(*pw);
	*pw = 0;
}

size_t cwindow_len(cwindow_t w)
{
	return w->len;
}


bool cwindow_get(cwindow_t w, size_t index,size_t num, uint8_t* buf)
{
	if(index +num > w->len )
	{
		errno = EINVAL;
		return false;
	}
	
	int begin  = (w->start + index)%w->len;
	int n = num;
	int left = w->len-(w->start+index);
	if(left<num)
	{
		memcpy(buf,&w->buf[begin],sizeof(uint8_t)*left);
		n -= left;
		begin = 0;
		buf += left;
	}
	memcpy(buf,&w->buf[begin],sizeof(uint8_t)*n);

	return true;
}

bool cwindow_put(cwindow_t w,size_t index,size_t num,uint8_t* buf)
{
	
	if(num+index > w->len )
	{
		errno = EINVAL;
		return false;
	}
	
	int begin  = (w->start + index)%w->len;
	int n = num;
	int left = w->len-(w->start+index);
	if(left<num)
	{
		memcpy(&w->buf[begin],buf,sizeof(uint8_t)*left);
		n -= left;
		begin = 0;
		buf += left;
	}
	memcpy(&w->buf[begin],buf,sizeof(uint8_t)*n);

	return true;
}

int cwindow_put_from_file(cwindow_t w,size_t num,size_t off,FILE* f)
{
	if(num+off > w->len )
	{
		errno = EINVAL;
		return -1;
	}
	
	int begin  = (w->start + off)%w->len;
	int n = num;
	int left = w->len-(w->start+off);
	int read = 0;
	int ret;
	if(left<num)
	{
		ret = fread(&w->buf[begin],sizeof(uint8_t),left,f);
		if( ret != left)
			return ret;
		n -= left;
		read += ret;
		begin = 0;
	}
	ret = fread(&w->buf[begin],sizeof(uint8_t),n,f);
	read += ret;
	if( ret < 0 )
		return ret;

	return read;

}

bool cwindow_shiftl(cwindow_t w,size_t num)
{
	if(num >= w->len )
	{
		errno = EINVAL;
		return false;
	}

	w->start = (w->start+num)%w->len;

	return true;
}


bool cwindow_shiftr(cwindow_t w,size_t num)
{
	if(num >= w->len )
	{
		errno = EINVAL;
		return false;
	}

	w->start = (w->start-num)%w->len;
	w->start = (w->start<0)?w->start + w->len:w->start;

	return true;
}
