#include <stdio.h>

#include "lz77.h"
#include "lzwindow.h"
#include "bitio.h"
#include "huffman.h"


//discrete logarithm base 2
static inline size_t ceil_log2(size_t n)
{
	int ceil = 0;
	int index = 0;

	while(n>1)
	{
		ceil |= 0x01&n;
		n = n>>1;
		index++;
	}
	return index+ceil;
}


int lz77encode(FILE* in,FILE* out,struct lz77opts opts)
{
	lzwindow_t l;
	lzwindow_init(in,opts.windowLength,opts.lookaheadLength,&l);

	fwrite(MAGIC,1,MAGIC_LEN,out);

	bitio_t bout;
	bitio_attach(&bout,out);
	
	bitbuffer_t indexbits = ceil_log2(opts.windowLength);
	bitio_write(bout,indexbits,8);
	bitbuffer_t lenbits = ceil_log2(opts.lookaheadLength-LAST_KEY+1);
	bitio_write(bout,lenbits,8);

	struct match m;
	uint8_t s ;
	bitstring_t bs;
	while(l.lleft!=0)
	{
		lzsearch(&l,&m);

		if(m.len == 0 || m.len == 1)
		{
			encode(&bs,0);
			bitio_write(bout,bs.value,bs.len);
			lzwindow_getl(&l,0,&s);
			bitio_write(bout,s,8);
			m.len = 1;
		}
		else if(m.len>=LAST_KEY)
		{
			encode(&bs,LAST_KEY);
			bitio_write(bout,bs.value,bs.len);
			bitio_write(bout,m.len-LAST_KEY,lenbits);
			bitio_write(bout,m.index,indexbits);
		}
		else
		{
			encode(&bs,m.len);
			bitio_write(bout,bs.value,bs.len);
			bitio_write(bout,m.index,indexbits);
		}

		lzwindow_advance(&l,m.len);
	}
		
	encode(&bs,EOF_KEY);
	bitio_write(bout,bs.value,bs.len);
	bitio_detach(&bout);
	lzwindow_free(&l);	

	return 0;
}

