#include <stdlib.h>
#include <errno.h>
#include <string.h>

#include "lzwindow.h"



//functions to convert index in the array from real position  to the shifted one and
//vice versa
static inline size_t real2off(const lzwindow_t* lz,size_t real)
{
	int len = lz->wlen + lz->llen;
	return (real-lz->off+len)%(len);
}
static inline size_t off2real(const lzwindow_t* lz,size_t off)
{
	return (off+lz->off)%(lz->wlen+lz->llen);
}

//add node in the assumption it is not already in the tree
static void  add_node(lzwindow_t* lz, ssize_t node) 
{
	lz->tree_size ++;
	if(lz->root == NULL_NODE)
	{
		lz->root = node;
		return;
	}
	ssize_t p = lz->root;
	int i = 0;
	while(true)
	{
		if(lz->win[node+i]<lz->win[p+i] || i == lz->lleft)
		{
			if(lz->tree[p].left==NULL_NODE)
			{
				lz->tree[p].left = node;
				break;
			}
			p = lz->tree[p].left;
			i = 0;
		}
		else if(lz->win[node+i]>lz->win[p+i])
		{
			if(lz->tree[p].right==NULL_NODE)
			{
				lz->tree[p].right = node;
				break;
			}
			p = lz->tree[p].right;
			i = 0;
		}
		else
		{
			i++;
		}


	}

	lz->tree[node].parent = p;
	lz->tree[node].left = NULL_NODE;
	lz->tree[node].right = NULL_NODE;
}

//replaces node with other and fixes parent and children
//to be used only if node has one or zero children and other is not in the tree
static void replace_node(lzwindow_t* lz, ssize_t node, ssize_t other)
{
	ssize_t parent = lz->tree[node].parent;
	if(parent == NULL_NODE)
	{
		lz->root = other;
	}
	else
	{
		if(lz->tree[parent].left == node)
		{
			lz->tree[parent].left = other;
		}
		else
		{
			lz->tree[parent].right = other;
		}
	}

	if(other!=NULL_NODE)
	{
		lz->tree[other].parent = parent;
	}

	size_t left = lz->tree[node].left;
	if(left != NULL_NODE && left != other)
	{
		lz->tree[left].parent = other;
	}

	size_t right = lz->tree[node].right;
	if(right != NULL_NODE && right != other)
	{
		lz->tree[right].parent = other;
	}

	lz->tree[node].parent = NULL_NODE;
	lz->tree[node].left = NULL_NODE;
	lz->tree[node].right = NULL_NODE;
}

//finds the node in-order-predecessor in the tree
static ssize_t in_order_predecessor(lzwindow_t* lz, ssize_t node)
{
	ssize_t iter = lz->tree[node].left;
	while(lz->tree[iter].right!=NULL_NODE)
	{
		iter = lz->tree[iter].right;
	}
	return iter;
}

//finds the node in-order-successor in the tree
static ssize_t in_order_successor(lzwindow_t* lz, ssize_t node)
{
	ssize_t iter = lz->tree[node].right;
	while(lz->tree[iter].left!=NULL_NODE)
	{
		iter = lz->tree[iter].left;
	}
	return iter;
}

//removes node from the tree
//3 cases:
//			-0 children
//			-1 child
//			-2 children
static void remove_node(lzwindow_t *lz,ssize_t node)
{
	//no left child
	if(lz->tree[node].left == NULL_NODE)
	{
		replace_node(lz,node,lz->tree[node].right);
	}
	//no right child
	else if(lz->tree[node].right == NULL_NODE)
	{
		replace_node(lz,node,lz->tree[node].left);
	}
	//two children, need replacement
	else
	{
		ssize_t p;
		//try to avoid unbalanced tree
		if(node%2==0)
		{
			p = in_order_predecessor(lz,node);
			replace_node(lz,p,lz->tree[p].left);
		}
		else
		{
			p = in_order_successor(lz,node);
			replace_node(lz,p,lz->tree[p].right);
		}

		lz->tree[p].left = lz->tree[node].left;
		lz->tree[p].right = lz->tree[node].right;
		replace_node(lz,node,p);
	}

	lz->tree_size --;
}

bool lzwindow_init(FILE* f,size_t wlen, size_t llen,lzwindow_t* lz)
{
	lz->wlen = wlen;
	lz->llen = llen;
	lz->f = f;
	lz->off = 0;
	lz->root = NULL_NODE;
	lz->tree_size = 0;
	
	lz->win = (uint8_t*)calloc(wlen+llen,sizeof(uint8_t)); 
	if(lz->win == 0)
	{
		errno = ENOMEM;
		goto calloc_win_err;
	}
	memset(lz->win,0xff,llen+wlen);
	
	lz->tree = (node_t*)calloc(wlen+llen,sizeof(node_t)); 
	if(lz->tree == 0)
	{
		errno = ENOMEM;
		goto calloc_tree_err;
	}
	
	int i = 0;
	for(;i<wlen+llen;i++)
	{
		lz->tree[i].parent = NULL_NODE;
		lz->tree[i].right = NULL_NODE;
		lz->tree[i].left = NULL_NODE;
	}

	lz->lleft = fread(&lz->win[lz->wlen],1,lz->llen,f);

	return lz->lleft > 0;

calloc_tree_err:
	free(lz->win);
calloc_win_err:
	return false;
}

void lzwindow_free(lzwindow_t* lz)
{
	free(lz->win);
	free(lz->tree);
}

bool lzwindow_getw(const lzwindow_t* lz, size_t index, uint8_t* s)
{
	//the lookahead is considered part of the window
	if(index  >= lz->wlen+lz->llen )
	{
		errno = EINVAL;
		return false;
	}
	size_t pos = off2real(lz,index);
	*s =  lz->win[pos];

	return true;
}

bool lzwindow_getl(const lzwindow_t* lz, size_t index, uint8_t* s)
{
	if(index  >= lz->llen )
	{
		errno = EINVAL;
		return false;
	}
	size_t pos = off2real(lz,index+lz->wlen);
	*s =  lz->win[pos];

	return true;
}

int lzwindow_advance(lzwindow_t* lz, size_t num)
{
	if(num > lz->llen )
	{
		errno = EINVAL;
		return -1;
	}
	size_t len = lz->wlen+lz->llen;


	int i;
	for(i = 0;i < num; i++)
	{
		if(lz->tree_size == lz->wlen)
		{
			remove_node(lz,off2real(lz,0));
		}
		int ret = fgetc(lz->f);
		if(ret>=0)
		{
			lz->win[off2real(lz,0)] = ret;
		}
		else
		{
			lz->lleft--;
		}

		add_node(lz,off2real(lz,lz->wlen));

		lz->off = (lz->off+1)%(len);

	}

	return i;
}

//compares sub-window starting at index with lh buffer
//returns lenght of the common substring and stores in greater
//the comparison
static ssize_t match_cmp(lzwindow_t *lz,ssize_t index, bool* greater)
{
	if(index < 0 )
	{
		errno = EINVAL;
		return -1;
	}

	int i;
	uint8_t s = 0,w = 0;
	for(i = 0;i<lz->lleft;i++)
	{
		w = lz->win[i+index];
		lzwindow_getl(lz,i,&s);
		if(w!=s)
		{
			*greater = s>w;
			break;
		}
	}
	return i;
}

int lzsearch(lzwindow_t *lz,struct match* m)
{
	m->len = 0;
	m->index = -1;

	ssize_t i = lz->root;

	while(i!=NULL_NODE && m->len < lz->lleft && i < lz->wlen)
	{
		bool greater;
		ssize_t n = match_cmp(lz,i,&greater);
		if(n>m->len)
		{
			m->len = n;
			m->index = real2off(lz,i);
		}
		i = greater?lz->tree[i].right:lz->tree[i].left;

	}

	return m->len;
}



