#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>

#include "bitio.h"


#define BUFBITS (CHAR_BIT*sizeof(bitbuffer_t))

struct bitio
{
	bitbuffer_t buf;
	//bits already fetched from file (used for reading)
	size_t rbits;
	//bits still to be written to file (used for writing)
	size_t wbits;
	FILE* f;
};

void copybits(bitbuffer_t* dst,bitbuffer_t src, size_t offd, size_t offs,size_t num)
{
	bitbuffer_t mask = 0;
	int i;
	for(i = 0; i< num; i++)
	{
		mask = mask|(1<<i);
	}
	mask <<= offd;

	int bitoff = offd - offs;
	if(bitoff < 0)
	{
		src >>= -bitoff;
	}
	else
	{
		src <<= bitoff;
	}
	
	*dst |= mask&src;
	*dst &= (~mask)|src;

}

bool bitio_attach(bitio_t *pio, FILE* f)
{
	*pio = (bitio_t)calloc(1,sizeof(struct bitio));
	if(*pio == 0)
		goto calloc_err;

	(*pio)->rbits = 0;
	(*pio)->wbits = 0;
	(*pio)->buf = 0;
	(*pio)->f = f;

	return true;

calloc_err:
	errno = ENOMEM;
	return false;
}

void  bitio_detach(bitio_t *pio)
{
	if((*pio)->wbits!= 0)
	{
		//write pending bits and pad with zeros
		fputc((*pio)->buf,(*pio)->f);
	}

	free(*pio);
	*pio = 0;
}

int bitio_read(bitio_t io, bitbuffer_t* pbits,size_t num)
{
	int n = 0;
	int toread = 0;

	if(num > BUFBITS)
	{
		errno == ERANGE;
		return EOF;
	}
	for(n = num; n>0; n-=toread)
	{
		if(io->rbits == 0)
		{
			int ret = fgetc(io->f);
			if(ret == EOF)
			{
				return EOF;
			}
			io->buf = ret;
			io->rbits = 8;
		}
		toread = (n > io->rbits) ? io->rbits : n;
		int offd = n - toread;
		int offs = 8 - toread;
		copybits(pbits,io->buf,offd,offs,toread);
		io->rbits -= toread;
		io->buf <<= toread;
	}
	return num - n;
	
}
int bitio_write(bitio_t io, bitbuffer_t bits,size_t num)
{
	int n = 0;
	int towrite = 0;

	if(num > BUFBITS)
	{
		errno == ERANGE;
		return EOF;
	}

	for(n = num; n>0; n-=towrite)
	{
		if(io->wbits == 8)
		{
			int ret = fputc(io->buf,io->f);
			if(ret == EOF)
				return EOF;
			io->wbits = 0;
		}
		towrite = (n > 8-io->wbits) ? (8-io->wbits) : n;
		int offs =  n - towrite;
		int offd =  8 - towrite - io->wbits;
		copybits(&io->buf,bits,offd,offs,towrite);
		io->wbits += towrite;

	}
	return num - n;
}
