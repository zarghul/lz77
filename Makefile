#Variables
EXEC = lz77
SOURCES := $(wildcard *.c)

OBJDIR = build
DEPDIR = build/.deps

OBJS := $(patsubst %.c, $(OBJDIR)/%.o,$(SOURCES))
DEPS = $(DEPDIR)/$(*F)

CC = gcc
CFLAGS = -g -Wall #-Werror

MAKEDEPS = cp $(OBJDIR)/$(*F).d $(DEPS).P; \
           sed -e 's/\#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
               -e '/^$$/ d' -e 's/$$/ :/' < $(OBJDIR)/$(*F).d >> $(DEPS).P; \
           rm -f $(OBJDIR)/$(*F).d	


#python
PYTHON = python2
PYOUTDIR = $(OBJDIR)
PYSCRIPT = huffmangen.py
PYFREQS = freqs.txt
PYFILES = $(OBJDIR)/huffcodes.h $(OBJDIR)/hufftable.h $(OBJDIR)/huffdefs.h
PYCMD = $(PYTHON) $(PYSCRIPT) $(PYFREQS) $(PYFILES)

#Rules
all: $(OBJDIR)/$(EXEC)

$(OBJDIR):
	mkdir -p $@
$(DEPDIR):
	mkdir -p $@

$(OBJDIR)/%.o: %.c | $(OBJDIR) $(DEPDIR)
	$(CC) -c $(CFLAGS) $(LDFLAGS) -MD -o $@ $< 
	@$(MAKEDEPS)

$(OBJDIR)/$(EXEC): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^

clean: 
	rm -rf $(OBJDIR)

#Dependencies
-include $(SOURCES:%.c=$(DEPDIR)/%.P)

#additional rules/dependencies
$(PYFILES): $(PYFREQS) $(PYSCRIPT)
	$(PYCMD)

$(OBJDIR)/huffman.o: $(PYFILES)

