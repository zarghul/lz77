#include <stdint.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include "lz77.h"

//utility function
bool is_power_of_2 (unsigned int x)
{
  return ((x != 0) && ((x & (~x + 1)) == x));
}

//checks if the -w option is valid
//takes a string of the form n[KMG] and returns 
//the number in bytes or -1 if wrong format or not 
//power of 2
ssize_t check_windowl(char* str)
{
	unsigned int num;
	char base;
	int k = sscanf(str,"%u%c",&num,&base);
	if(k<1)
	{
		return -1;
	}
	if(!is_power_of_2(num))
	{
		return -1;
	}
	if(k==2)
	{
		switch(base)
		{
			case 'k':
			case 'K':
				num*=1024;
				break;
			case 'm':
			case 'M':
				num*=1024*1024;
				break;
			case 'g':
			case 'G':
				num*=1024*1024*1024;
				break;
			default:
				return -1;
		}
	}
	return num;
}

//checks if the -w option is valid
//takes a string representing the lenght in bytes 
//returns the number in bytes or -1 if wrong format 
ssize_t check_lookaheadl(char* str)
{
	char* endptr;
	ssize_t num = strtoul(optarg,&endptr,0);
	if(*endptr!='\0' || num <= 0)
	{
		return -1;
	}
	return num;
}

void print_help()
{
	fprintf(stderr,"\
Usage: lz77 [OPTION]...\n\
Compression/decompression utility based on the lz77 algorithm.\n\
\n\
  -c\t compress input into output\n\
  -d\t decompress input into output\n\
  -w\t set sliding window size. Accept K|M|G notation.\n\
  \t MUST be power of 2. [DEFAULT: 4K]\n\
  -l\t set lookahead buffer size.[DEFAULT: 16]\n\
  -i\t set input file. [DEFAULT stdin]\n\
  -o\t set output file. [DEFAULT stdout]\n\
  -h\t prints this help\n");
}

void print_opt_err(char* str)
{
	fprintf(stderr,"%s. Type -h for help\n",str);
}

enum mode_t
{
	compress,
	decompress,
	none
};

int main(int argc, char** argv)
{
	//default values for options
	ssize_t windowl = 4096;
	ssize_t lookaheadl = 16;
	FILE* in = stdin;
	FILE* out = stdout;
	mode_t mode = none;


	int c;
	//opterr = 0;
	while ((c = getopt (argc, argv, ":w:l:i:o:hcd")) != -1)
	{
		switch (c)
		{
			case 'w':
				windowl = check_windowl(optarg);
				if(windowl<=0)
				{
					print_opt_err("Wrong value for -w");
					exit(-1);
				}
				break;
			case 'l':
				lookaheadl = check_lookaheadl(optarg);
				if(lookaheadl<=0)
				{
					print_opt_err("Wrong value for -l");
					exit(-1);
				}
				break;
			case 'i':
				in = fopen(optarg,"rb");
				if(in==0)
				{
					print_opt_err("Error opening file for -i");
					exit(-1);
				}
				break;
			case 'o':
				out = fopen(optarg,"wb");
				if(out==0)
				{
					print_opt_err("Error opening file for -o");
					exit(-1);
				}
				break;
			case 'c':
				if(mode!=none)
				{
					print_opt_err("Specify EXACTLY one between -c and -d");
					exit(-1);
				}
				mode = compress;
				break;
			case 'd':
				if(mode!=none)
				{
					print_opt_err("Specify EXACTLY one between -c and -d");
					exit(-1);
				}
				mode = decompress;
				break;
			case 'h':
					print_help();
					exit(0);
			case '?':
					print_opt_err("Invalid arguments");
					exit(-1);
			default:
				abort();
		}
	}

	struct lz77opts opts;
	switch(mode)
	{
		case compress:
			opts.windowLength = windowl;
			opts.lookaheadLength = lookaheadl;
			lz77encode(in,out,opts);
			break;
		case decompress:
			lz77decode(in,out);  
			break;
		case none:
				print_opt_err("Specify EXACTLY one between -c and -d");
				exit(-1);
	}


	fclose(in);
	fclose(out);

	return 0;
}
