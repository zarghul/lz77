#ifndef __HUFFMAN_H__
#define __HUFFMAN_H__

#include "build/huffdefs.h"

//structure that represent a string of bit of len lenght (up to CHAR_BITS)
struct bitstring
{
	uint8_t value;
	uint8_t len;
};

typedef struct bitstring bitstring_t;

//encode symbol to huffman representation
int encode(bitstring_t* code, uint8_t symbol);
//decode symbol using constant time decoding table
int decode(uint16_t stream, uint8_t* symbol);


#endif /* __HUFFMAN_H__ */

