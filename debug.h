#ifndef _DEBUG_H_
#define _DEBUG_H_

#ifdef DEBUG 
	#define DEBUG_TEST 1
#else
	#define DEBUG_TEST 0
#endif

#define DPRINTF_HACK(fmt,...) \
	do { if (DEBUG_TEST) fprintf(stderr,"[%s][%s][%d]" fmt "%s",__FILE__,__func__,__LINE__, __VA_ARGS__); } while (0)

#define DPRINTF(...) DPRINTF_HACK(__VA_ARGS__,"\n")

#define DPRINTINT(n) DPRINTF(#n " = %d",(int)n); 

#endif // _DEBUG_H_
