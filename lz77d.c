#include <string.h>
#include <errno.h>

#include "cwindow.h"
#include "bitio.h"
#include "lz77.h"
#include "huffman.h"


//read a code from the bitio but takes bits from buf first. if it reads more bits
//than necessary, it stores them in buf
int code_read(bitio_t bin,uint8_t* code,bitbuffer_t* buf, size_t* bsize)
{
	int toread = MAXBITS - *bsize;
	bitbuffer_t tmp = 0;

	int ret = bitio_read(bin,&tmp,toread);
	if(ret==EOF)
	{
		return ret;
	}

	copybits(&tmp,*buf,toread,0,*bsize);


	int read = decode(tmp,code);
	*bsize = MAXBITS - read;
	*buf = tmp;
	
	return ret;
}
//read from the bitio but takes bits from buf first
int buffered_read(bitio_t bin,bitbuffer_t* data, size_t num,bitbuffer_t* buf, size_t* bsize)
{
	bitbuffer_t tmp = 0;
	int toread = num - *bsize;

	int ret = 0;
	if(toread>0)
	{
		ret = bitio_read(bin,&tmp,toread);
		if(ret==EOF)
		{
			return ret;
		}

		copybits(&tmp,*buf,toread,0,*bsize);
		*bsize = 0;
		*data = tmp;
	}
	else
	{
		copybits(&tmp,*buf,0,0,num);
		*bsize -= num;
	}
	return ret;
}

//main decoding function
int lz77decode(FILE* in,FILE* out)
{
	//check magic number
	char magic_check[MAGIC_LEN];
	fread(magic_check,1,MAGIC_LEN,in);
	if(memcmp(magic_check,MAGIC,MAGIC_LEN)!=0)
	{
		return WRONG_TYPE;
	}

	//attach bitio
	bitio_t bin;
	bitio_attach(&bin,in);

	//read number of bits for indexing the window
	bitbuffer_t indexbits = 0;
	bitio_read(bin,&indexbits,8);

	//read the number of bits for the match lenght
	bitbuffer_t lenbits = 0;
	bitio_read(bin,&lenbits,8);

	//alloc the circular window
	cwindow_t w;
	int len = 2<<(indexbits-1);
	uint8_t init[len];
	memset(init,0x0,len);
	cwindow_alloc(&w,len,init);


	bitbuffer_t index;
	uint8_t num = 0;
	bitbuffer_t buf = 0;
	size_t bufbits = 0;
	uint8_t s;
	static int max = 0;
	while(1)
	{
		if(code_read(bin,&num,&buf,&bufbits)==EOF)
		{
			break;
		}
		if(num == 0)
		{
			bitbuffer_t tmp = 0;
			if(buffered_read(bin,&tmp,8,&buf,&bufbits)==EOF)
			{
				break;
			}
			s = tmp;

			fwrite(&s,1,1,out);
			num = 1;
			cwindow_shiftl(w,1);
			cwindow_put(w,len-1,1,&s); 
		}
		else if (num == EOF_KEY)
		{
			break;
		}
		else
		{
			bitbuffer_t n = 0;
			if(num==LAST_KEY)
			{
				if(buffered_read(bin,&n,lenbits,&buf,&bufbits)==EOF)
				{
					break;
				}
				num += n;
				if(num>max)
				{
					max=num;
				}
			}

			if(buffered_read(bin,&index,indexbits,&buf,&bufbits)==EOF)
			{
				break;
			}
			int i;
			for(i = 0; i < num;i++)
			{
				cwindow_get(w,index,1,&s);
				fwrite(&s,1,1,out);
				cwindow_shiftl(w,1);
				cwindow_put(w,len-1,1,&s); 

			}
		}
	}
	bitio_detach(&bin);
	cwindow_free(&w);

	return 0;
}
