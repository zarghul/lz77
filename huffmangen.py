#!/usr/bin/env python

import sys

from heapq import *

class Node:
    value = None
    left = None
    right = None
    weight = 0

    def __init__(self,value=None,weight=0):
        self.value = value
        self.weight = weight

    def setChildren(self,l,r):
        self.left = l
        self.right = r

    def __repr__(self):
        return "("+str(self.value)+","+str(self.weight)+")"+"left{"+repr(self.left)+"} right{"+repr(self.right)+"}"

    def __lt__(self,a):
        return self.weight < a.weight


hufffreqs = sys.argv[1]
huffcodes = sys.argv[2]
hufftable = sys.argv[3]
huffdefs = sys.argv[4]

freqs =  dict([[int(x) for x in line.strip().split()] for line in open(hufffreqs)])
EOFkey = max(freqs.keys())+1
freqs[EOFkey] = 0

nodes = [Node(i,w) for i,w in freqs.iteritems()]

heapify(nodes)

while(len(nodes)>1):
    l =  heappop(nodes)
    r =  heappop(nodes)
    n = Node(None,r.weight+l.weight)
    n.setChildren(l,r)
    heappush(nodes,n)


codes = {}
def codeIt(s, node):
    if node.value != None:
        if not s:
            codes[node.value] = "0"
        else:
            codes[node.value] = s
    else:
        codeIt(s+"0", node.left)
        codeIt(s+"1", node.right)

codeIt("0b",nodes[0])

maxbits = max([len(bits) for bits in codes.values()]) - 2
tab = {}

for k,v in codes.iteritems():
    indexBase = int(v,2)
    indexBits = len(v)-2
    freebits = maxbits - indexBits
    for i in range(1<<freebits):
        tab[i+(indexBase << freebits)] = (k,indexBits)

    
definesFile = open(huffdefs,"w")
definesFile.write("#define NCODES "+str(EOFkey+1)+"\n")
definesFile.write("#define MAXBITS "+str(maxbits)+"\n")
definesFile.write("#define EOF_KEY "+str(EOFkey)+"\n")
definesFile.write("#define LAST_KEY "+str(EOFkey-1)+"\n")
definesFile.close()

codesFile = open(huffcodes,"w")
for k,v in codes.iteritems():
    codesFile.write("["+str(k)+"]={"+v+","+str(len(v)-2)+"},\n")
codesFile.close()

tableFile = open(hufftable,"w")
for k,v in tab.iteritems():
    tableFile.write("["+str(k)+"]={"+str(v[0])+","+str(v[1])+"},\n")
tableFile.close()

