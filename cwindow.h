#ifndef _CWINDOW_H_
#define _CWINDOW_H_

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

//forward declaration of pointer for incapsulation
typedef struct cwindow* cwindow_t;

//function for allocating a circular window
//return true on success, false otherwise
bool cwindow_alloc(cwindow_t* pw,size_t len,uint8_t* init);
//function for freeing a circular window
void cwindow_free(cwindow_t* pw);
	
//returns window lenght
size_t cwindow_len(cwindow_t w);
	
//copies part of the window content in the given buf
//performs bound check
bool cwindow_get(cwindow_t w, size_t index, size_t num,  uint8_t* buf);
//copies the given buf in the window starting from index
//performs bound check
bool cwindow_put(cwindow_t w,size_t index,size_t num,uint8_t* buf);
//fetch data from given file
//returns -1 on errors, bytes effetively read otherwise
int cwindow_put_from_file(cwindow_t w,size_t num,size_t off,FILE* f);
	
//shift the window to the left
//returns false if shifting more than lenght
bool cwindow_shiftl(cwindow_t w,size_t num);
//shift the window to the right
//returns false if shifting more than lenght
bool cwindow_shiftr(cwindow_t w,size_t num);

#endif //_CWINDOW_H_
