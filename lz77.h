
#ifndef __LZ77_H__
#define  __LZ77_H__

#include <stdio.h>

//magic number for file type
#define MAGIC "lz77y"
#define MAGIC_LEN  5

#define WRONG_TYPE -1

struct lz77opts
{
	int windowLength;
	int lookaheadLength;
};

//encode function
int lz77encode(FILE* in,FILE* out,struct lz77opts opts);
//decode function
//checks file for magic number,
//returns WRONG_TYPE if not present
int lz77decode(FILE* in,FILE* out);

#endif /* __LZ77_H__ */
