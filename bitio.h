#ifndef __BITIO_H__
#define  __BITIO_H__

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>

//forward declaration for incapsulation
typedef struct bitio* bitio_t;

//type for storing bits
//abstraction from actual support type
typedef uint32_t bitbuffer_t;

//copy num bits ftom src (starting at offs) to dst(starting from offd)
void copybits(bitbuffer_t* dst,bitbuffer_t src, size_t offd, size_t offs,size_t num);

//attach bitio struct to given file
//file should not be used alone until detach
//return true upon success, false otherwise
bool bitio_attach(bitio_t *pio, FILE* f);
//detach structure from file and flush write buffer.
//file can be used again or closed
void bitio_detach(bitio_t *pio);
//read num bits, store in pbits
//returns actual number of bits read
int bitio_read(bitio_t io, bitbuffer_t* pbits,size_t num);
//write num bits from pbits
//returns actual number of bits written
int bitio_write(bitio_t io, bitbuffer_t pbits,size_t num);

#endif /* __BITIO_H__ */

